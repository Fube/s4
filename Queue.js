const { LinkedList } = require("./linked");

/**
 * A queue is a datastructure in which you add and remove in order (FIFO)
 * Ex: add(1), add(2), add(3)
 * remove() -> returns 1
 * remove() -> returns 2
 * remove() -> returns 3
 */

class Queue extends LinkedList {}
