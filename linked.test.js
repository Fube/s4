const { LinkedList } = require("./linked");

test("creates a LinkedList", () => {
    const VAL = 1;
    const lList = new LinkedList(VAL);
    expect(lList.head).toBe(VAL);
});

test("get tail of LinkedList", () => {
    const HEAD = 1;
    const TAIL = 2;

    const lList = new LinkedList(HEAD);
    lList.add(TAIL);
    expect(lList.tail).toBe(TAIL);
});

test("iterate through LinkedList", () => {
    const DATA = [1, 2];

    const lList = new LinkedList(DATA[0]);
    for (let i = 1; i < DATA.length; i++) {
        lList.add(DATA[i]);
    }

    let i = 0;
    for (const data of lList) {
        expect(DATA[i]).toBe(data);
        i++;
    }
});
