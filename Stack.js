const { LinkedList } = require("./linked");

/**
 * A stack is a datastructure in which you add but remove in reverse order (FILO)
 * Ex: add(1), add(2), add(3)
 * remove() -> returns 3
 * remove() -> returns 2
 * remove() -> returns 1
 */

class Stack extends LinkedList {}
