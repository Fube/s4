// Node1 {data: 10, next: Node3}
// Node2 {data: 123, next: Node3}
// Node3 {data: 9, next: Node4}
// Node4 {data: 3, next: null}

/**
 * A single Node in a {@link LinkedList}
 */
class _Node {
    /**
     * Create a Node
     * @param {*} data - Data to hold
     * @param {Node} next - Reference to next Node in the list
     */
    constructor(data, next) {
        this.data = data;
        this.next = next;
    }
}

class LinkedList {
    /**
     * @type {_Node} - Reference to the first Node in the list
     */
    #head;

    constructor(data) {
        this.#head = new _Node(data, null); // Null pointer indicates end of list
    }

    /**
     * Finds the last Node (the tail) in the list
     * @returns {_Node} Tail of list
     */
    #findLast() {
        if (this.#head.next === null) {
            return this.#head;
        }

        let current = this.#head;

        while (current.next !== null) {
            current = current.next;
        }

        return current;
    }

    add(data) {
        const tail = this.#findLast();
        tail.next = new _Node(data, null);
    }

    // TODO
    remove(data) {}

    // TODO
    addAt(index, data) {}

    get head() {
        return this.#head.data;
    }

    get tail() {
        return this.#findLast().data;
    }

    *[Symbol.iterator]() {
        if (this.#head === null) {
            yield;
        }

        let current = this.#head;
        while (current !== null) {
            yield current.data;
            current = current.next;
        }
    }
}

module.exports = {
    Node: _Node,
    LinkedList,
};
